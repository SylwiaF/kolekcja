package com.example.kolekcje.Classes

import android.content.Context
import android.util.Log
import com.example.kolekcje.Models.ObjectDescription
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException

object ObjectDB {
    private var listOfObjects = mutableListOf<ObjectDescription>()
    private var isDataReady = false
    private var pointer = 0
    private var sortBy = "name"
    private const val fileName = "plik.txt"
    lateinit var mainContext: Context

    fun get(context: Context): ObjectDescription {
        mainContext = context
        if (!isDataReady) {
            load()
        }
        return listOfObjects[pointer]
    }

    private fun load() {
        var line: List<String>
        var objectDesc: ObjectDescription
        var id = 1

        listOfObjects.clear()

        File(mainContext.getExternalFilesDir(null), fileName).forEachLine {
            line = it.split(";")
            objectDesc = ObjectDescription(id, line[0], line[1], line[2].toInt(), line[3].toDouble())
            listOfObjects.add(objectDesc)
            id++
        }

        sort()
        pointer = 0
        isDataReady = true
    }

    fun pointerNext(): Boolean {
        if (pointer < listOfObjects.size - 1) {
            pointer++
            return true
        }
        return false
    }

    fun pointerPrevious(): Boolean {
        if (pointer > 0) {
            pointer--
            return true
        }
        return false
    }

    fun add(element: ObjectDescription, context: Context) {
        mainContext = context
        element.id = getNextId()
        listOfObjects.add(element)
        pointer = listOfObjects.size - 1
        save()
    }

    fun replace(element: ObjectDescription, context: Context) {
        listOfObjects.removeAt(pointer)
        add(element, context)
    }

    private fun save() {
        var content = ""
        listOfObjects.forEach {
            content =
                content + it.name + ";" + it.category + ";" + it.amount + ";" + it.price + "\n"
        }
        Log.i("Dane z content", content)
        try {
            File(mainContext.getExternalFilesDir(null), fileName).writeText(content)


        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        isDataReady = true
    }

    fun delete() {
        listOfObjects.removeAt(pointer)
        save()
        pointerPrevious()
    }

    private fun sort() {
        when (sortBy) {
            "name" -> listOfObjects.sortBy { it.name }
            "category" -> listOfObjects.sortBy { it.category }
            "amount" -> listOfObjects.sortBy { it.amount }
            "price" -> listOfObjects.sortBy { it.price }
        }
        pointer = 0
    }

    fun sortBy(order: String) {
        when (order) {
            "name" -> sortBy = "name"
            "category" -> sortBy = "category"
            "amount" -> sortBy = "amount"
            "price" -> sortBy = "price"
        }
        sort()
    }

    private fun getNextId(): Int {
        var id = 0
        listOfObjects.forEach {
            if (it.id > id) {
                id = it.id
            }
        }
        return id++
    }

    fun changePrice(percent: Int){
        listOfObjects.forEach {
            it.price = it.price + (it.price * (percent.toDouble() / 100))
        }
        save()
    }

    fun isSortedByName(): Boolean = sortBy == "name"
    fun isSortedByCategory(): Boolean = sortBy == "category"
    fun isSortedByAmount(): Boolean = sortBy == "amount"
    fun isSortedByPrice(): Boolean = sortBy == "price"


}