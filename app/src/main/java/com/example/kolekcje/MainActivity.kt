package com.example.kolekcje

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.kolekcje.Classes.ObjectDB

class MainActivity : AppCompatActivity() {

    lateinit var name: TextView
    lateinit var category: TextView
    lateinit var amount: TextView
    lateinit var price: TextView

    lateinit var nameText: TextView
    lateinit var categoryText: TextView
    lateinit var amountText: TextView
    lateinit var priceText: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        name = findViewById(R.id.name_edit) as TextView
        category = findViewById(R.id.category_edit) as TextView
        amount = findViewById(R.id.amount_edit) as TextView
        price = findViewById(R.id.price_edit) as TextView
        val percent = findViewById(R.id.percent_text) as EditText


        nameText = findViewById(R.id.name_text_main) as TextView
        categoryText = findViewById(R.id.category_text_main) as TextView
        amountText = findViewById(R.id.amount_text_main) as TextView
        priceText = findViewById(R.id.price_text_main) as TextView



        val addButton = findViewById<Button>(R.id.add_button)
        val nextButton = findViewById<Button>(R.id.next_button_edit)
        val previousButton = findViewById<Button>(R.id.previous_button_edit)
        val editButton = findViewById<Button>(R.id.cancel_button)
        val deleteButton = findViewById<Button>(R.id.delete_button)

        val percentButton = findViewById<Button>(R.id.percent_button)




        showData()

        nameText.setOnClickListener {
            ObjectDB.sortBy("name")
            showData()
        }

        categoryText.setOnClickListener {
            ObjectDB.sortBy("category")
            showData()
        }

        amountText.setOnClickListener {
            ObjectDB.sortBy("amount")
            showData()
        }

        priceText.setOnClickListener {
            ObjectDB.sortBy("price")
            showData()
        }

        percentButton.setOnClickListener {
            ObjectDB.changePrice(percent.text.toString().toInt())
            percent.setText("")
            showData()
        }

        deleteButton.setOnClickListener {
            ObjectDB.delete()
            showData()
        }

        nextButton.setOnClickListener {
            if (ObjectDB.pointerNext()) {
                showData()
            }
        }

        previousButton.setOnClickListener {
            if (ObjectDB.pointerPrevious()) {
                showData()
            }
        }

        editButton.setOnClickListener {
            val intent = Intent(this, EditActivity::class.java)
            intent.putExtra("edit", true)
            startActivity(intent)
        }




        addButton.setOnClickListener {
            val intent = Intent(this, EditActivity::class.java)
            intent.putExtra("edit", false)
            startActivity(intent)
        }


    }

    fun showData() {
        val element = ObjectDB.get(this)

        name.setText(element.name)
        category.setText(element.category)
        amount.setText(element.amount.toString())
        price.setText(element.price.toString())


        if (ObjectDB.isSortedByName()) {
            nameText.setTypeface(null, Typeface.BOLD)
        } else {
            nameText.setTypeface(null, Typeface.NORMAL)
        }

        if (ObjectDB.isSortedByCategory()) {
            categoryText.setTypeface(null, Typeface.BOLD)
        } else {
            categoryText.setTypeface(null, Typeface.NORMAL)
        }

        if (ObjectDB.isSortedByAmount()) {
            amountText.setTypeface(null, Typeface.BOLD)
        } else {
            amountText.setTypeface(null, Typeface.NORMAL)
        }

        if (ObjectDB.isSortedByPrice()) {
            priceText.setTypeface(null, Typeface.BOLD)
        } else {
            priceText.setTypeface(null, Typeface.NORMAL)
        }
    }
}