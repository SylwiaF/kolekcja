package com.example.kolekcje

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.kolekcje.Classes.ObjectDB
import com.example.kolekcje.Models.ObjectDescription

class EditActivity : AppCompatActivity() {

    lateinit var name: EditText
    lateinit var category: EditText
    lateinit var amount: EditText
    lateinit var price: EditText
    var edit = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)

        edit = intent.getBooleanExtra("edit", false)

        name = findViewById(R.id.name_edit) as EditText
        category = findViewById(R.id.category_edit) as EditText
        amount = findViewById(R.id.amount_edit) as EditText
        price = findViewById(R.id.price_edit) as EditText

        if (edit) {
            showData()
        }

        val saveButton = findViewById<Button>(R.id.save_button)
        val cancelButton = findViewById<Button>(R.id.cancel_button)

        saveButton.setOnClickListener {

            val nameToFile: String = name.text.toString()
            val categoryToFile: String = category.text.toString()
            val amountToFile: Int = amount.text.toString().toInt()
            val priceToFile: Double = price.text.toString().toDouble()

            if (edit) {
                ObjectDB.replace(
                    ObjectDescription(
                        0,
                        nameToFile,
                        categoryToFile,
                        amountToFile,
                        priceToFile
                    ), this
                )
            } else {
                ObjectDB.add(
                    ObjectDescription(
                        0,
                        nameToFile,
                        categoryToFile,
                        amountToFile,
                        priceToFile
                    ), this
                )
            }

            Toast.makeText(applicationContext, "Dane zostały zapisane", Toast.LENGTH_LONG).show()

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)

        }

        cancelButton.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }

    fun showData() {
        val element = ObjectDB.get(this)

        name.setText(element.name)
        category.setText(element.category)
        amount.setText(element.amount.toString())
        price.setText(element.price.toString())

    }
}