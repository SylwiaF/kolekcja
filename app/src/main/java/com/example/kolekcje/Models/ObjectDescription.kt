package com.example.kolekcje.Models

data class ObjectDescription (
    var id: Int = 0,
    val name: String? = null,
    val category: String? = null,
    val amount: Int = 0,
    var price: Double = 0.0
) {
    override fun toString(): String {
        return "name: $name; category: $category; amount: $amount; price: $price"
    }

}